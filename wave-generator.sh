#!/bin/bash

if ! [ -f "${1}" ]; then
	fullpath=`readlink -f "${1}"`
	echo "File \"${fullpath}\" not found"
	exit
fi

json=`cat "${1}"`

text_offset="80"
height_offset="5"
signal_short="5"
signal_long="10"
signal_length=$(($signal_short+$signal_long))
signal_height="30"

pins_length=`echo "${json}" | jq ".pins | length"`
i="0"
while [ "${i}" -lt "${pins_length}" ]; do
	name=`echo "${json}" | jq -r ".pins[${i}].name"`
	color=`echo "${json}" | jq -r ".pins[${i}].color"`
	direction=`echo "${json}" | jq -r ".pins[${i}].direction"`
	echo "name: ${name}, color: ${color}, direction: ${direction}"
	dash="0"
	if [ "${direction}" -eq "0" ]; then
		dash="3 2"
	fi
	length=`echo "${json}" | jq ".pins[${i}].signal | length"`
	j="0"
	cross="0"
	polyline0=""
	polyline1=""
	printf "signal:"
	while [ "${j}" -lt "${length}" ]; do
		x1=$(($j*$signal_length))
		x2=$(($x1+$signal_long))
		signal=`echo "${json}" | jq -r ".pins[${i}].signal[${j}]"`
		printf " ${signal}"
		newline=""
		if [ $(($j%16)) -eq "0" ]; then
			newline=$'\n'
		fi
		if [ "${signal}" -eq "0" ]; then
			polyline0+="${newline} ${x1},${signal_height} ${x2},${signal_height}"
			polyline1+="${newline} ${x1},${signal_height} ${x2},${signal_height}"
		elif [ "${signal}" -eq "1" ]; then
			polyline0+="${newline} ${x1},0 ${x2},0"
			polyline1+="${newline} ${x1},0 ${x2},0"
		else
			if [ "${signal}" -eq "2" ]; then
				cross=$(($cross+1))
			fi
			if [ $(($cross%2)) -eq "0" ]; then
				polyline0+="${newline} ${x1},${signal_height} ${x2},${signal_height}"
				polyline1+="${newline} ${x1},0 ${x2},0"
			else
				polyline0+="${newline} ${x1},0 ${x2},0"
				polyline1+="${newline} ${x1},${signal_height} ${x2},${signal_height}"
			fi
		fi
		j=$(($j+1))
	done
	echo ""
	svg_g+="\n		<g transform='translate(0,"$(($i*($signal_height+$height_offset)+$height_offset))")'>"
	svg_g+="\n			<text x='${text_offset}' y='"$(($signal_height-$height_offset))"' fill='${color}'>${name}</text>"
	svg_g+="\n			<g transform='translate(${text_offset},0)' fill='none' stroke='${color}' stroke-dasharray='${dash}'>"
	svg_g+="\n				<polyline points='${polyline0:1}'/>"
	svg_g+="\n				<polyline points='${polyline1:1}'/>"
	svg_g+="\n			</g>"
	svg_g+="\n		</g>"
	i=$(($i+1))
done

width=$(($text_offset+$j*$signal_length))
height=$(($i*($signal_height+$height_offset)+$height_offset))

grid_lines=""
signal_lines=""
i="0"
while [ "${i}" -lt "${length}" ]; do
	grid_lines+="\n		<polyline points='"$(($i*$signal_length))",0 "$(($i*$signal_length))",${height}'/>"
	grid_lines+="\n		<polyline points='"$(($i*$signal_length+$signal_long))",0 "$(($i*$signal_length+$signal_long))",${height}'/>"
	if [ "${i}" -gt "0" ] && [ $(($i%2)) -eq "0" ]; then
		signal_x=`echo "${i} * ${signal_length} - 2.5" | bc`
		if [ $(($i%16)) -eq "0" ]; then
			signal_lines+="\n		<polyline points='${signal_x},0 ${signal_x},${height}' stroke='#FF0000'/>"
		else
			signal_lines+="\n		<polyline points='${signal_x},0 ${signal_x},${height}'/>"
		fi
	fi
	i=$(($i+1))
done

svg+=""
svg+="<svg viewBox='0 0 ${width} ${height}' width='${width}' height='${height}' xmlns='http://www.w3.org/2000/svg'>"
svg+="\n	<rect width='${width}' height='${height}' fill='#FFFF00'/>"
#svg+="\n	<g transform='translate(${text_offset},0)' fill='none' stroke='#000000' stroke-opacity='0.2'>"
#svg+="${grid_lines}"
#svg+="\n	</g>"
svg+="\n	<g text-anchor='end' font-family='OcrA' font-size='30'>"
svg+="${svg_g}"
svg+="\n	</g>"
svg+="\n	<g transform='translate(${text_offset},0)' fill='none' stroke='#0000FF'>"
svg+="${signal_lines}"
svg+="\n	</g>"
svg+="\n</svg>"

filename=`basename "${1}"`
printf "${svg}" | tr "'" '"' >`echo "${filename}" | sed -r 's/\.json$/.svg/g'`